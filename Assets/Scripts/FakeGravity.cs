﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeGravity : MonoBehaviour {

    public float moveSpeed = 6.0f;
    public float turnSpeed = 90.0f;
    public float lerpSpeed = 10.0f;
    public bool isGrounded;
    public float gravity = 10.0f;
    public float deltaGround = 0.2f;

    private Collider coll;
    private Rigidbody rb;
    private Ray ray;
    private RaycastHit hit;
    private Vector3 myNormal;
    private float distGround;
    private Vector3 surfaceNormal;

    private void Start() {
        coll = GetComponent<Collider>();
        rb = GetComponent<Rigidbody>();

        myNormal = transform.up;
        distGround = Vector3.down.magnitude;
    }

    private void FixedUpdate() {
        // apply constant weight force according to character normal
        rb.AddForce(-gravity * rb.mass * myNormal);
    }

    private void Update() {
        // movement code - turn left/right with Horizontal axis:
        transform.Rotate(0, Input.GetAxis("Horizontal") * turnSpeed * Time.deltaTime, 0);
        // update surface normal and isGrounded:
        ray = new Ray(transform.position, -myNormal); // cast ray downwards
        Debug.DrawRay(transform.position, -myNormal, Color.red);
        if (Physics.Raycast(ray, out hit, distGround + deltaGround)) { // use it to update myNormal and isGrounded
            //isGrounded = hit.distance <= distGround + deltaGround;
            isGrounded = true;
            //Debug.Log(hit.distance + " <= " + (distGround + deltaGround));
            surfaceNormal = hit.normal;
        }
        else {
            isGrounded = false;
            // assume usual ground normal to avoid "falling forever"
            surfaceNormal = Vector3.up;
        }
        myNormal = Vector3.Lerp(myNormal, surfaceNormal, lerpSpeed * Time.deltaTime);
        // find forward direction with new myNormal:
        var myForward = Vector3.Cross(transform.right, myNormal);
        // align character to the new myNormal while keeping the forward direction:
        var targetRot = Quaternion.LookRotation(myForward, myNormal);
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, lerpSpeed * Time.deltaTime);
        // move the character forth/back with Vertical axis:
        transform.Translate(0, 0, Input.GetAxis("Vertical") * moveSpeed * Time.deltaTime);
    }




}
